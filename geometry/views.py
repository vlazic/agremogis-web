from tempfile import NamedTemporaryFile

from django.core.files import File
from django.shortcuts import render
from django.http import HttpResponse

from agremogis import Gis, PARSERS

from .forms import GeometryForm


def handle_uploaded_file(uploaded_file, action, action_type):
    file_extension = uploaded_file.name.split(".")[-1]

    with NamedTemporaryFile(suffix='.' + file_extension) as tmp_file:
        for chunk in uploaded_file.chunks():
            tmp_file.write(chunk)

        gis = Gis(tmp_file.name)

        if action == 'convert':
            return gis.export_to(action_type)

        elif action == 'report':
            return gis.generate_report(action_type)


def form_view(request, *args, **kwargs):
    if request.method == 'POST':
        form = GeometryForm(request.POST, request.FILES)

        if form.is_valid():
            input_file = request.FILES['file']
            action = request.POST.get('action')

            if action == 'convertToGeoJson':
                response_file = handle_uploaded_file(
                    input_file, 'convert', 'GeoJSON')
                filename = 'GeoJSON.json'

            elif action == 'convertToShapefile':
                response_file = handle_uploaded_file(
                    input_file, 'convert', 'ShapeFile')
                filename = 'ShapeFile.zip'

            elif action == 'downloadJsonReport':
                response_file = handle_uploaded_file(
                    input_file, 'report', 'json')
                filename = 'report.json'

            elif action == 'downloadXmlReport':
                response_file = handle_uploaded_file(
                    input_file, 'report', 'xml')
                filename = 'report.xml'

            with open(response_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/octet-stream")
                response['Content-Disposition'] = 'inline; filename=' + filename

                return response

    else:
        form = GeometryForm()

    return render(request, 'form.html', {'form': form})
