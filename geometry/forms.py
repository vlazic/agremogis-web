from django import forms


class GeometryForm(forms.Form):
    actions = (
        ('convertToGeoJson', 'Convert file to GeoJson format'),
        ('convertToShapefile', 'Convert file to ShapeFile format'),
        ('downloadJsonReport', 'Download report in JSON format'),
        ('downloadXmlReport', 'Download report in XML format'),
    )

    file = forms.FileField(
        label="Upload your file:"
    )

    action = forms.ChoiceField(
        label="Pick your action:",
        required=True,
        widget=forms.RadioSelect(),
        choices=actions
    )
